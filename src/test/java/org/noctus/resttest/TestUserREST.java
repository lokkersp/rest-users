package org.noctus.resttest;

/**
 * Created by noctuam on 20.09.15.
 */
import org.junit.Test;
import org.noctus.model.Role;
import org.noctus.model.User;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

import static org.junit.Assert.*;


public class TestUserREST {
    private final RestTemplate restTemplate = new RestTemplate();

    private final String rolesUrl = "http://localhost:8080/roles";
    private final String usersUrl = "http://localhost:8080/users";

    @Test
    public void testCreateUserWithRole() throws Exception {
        final URI roleUri = restTemplate.postForLocation(rolesUrl, sampleRole()); // create role

        final URI userUri = new URI(usersUrl + "/" + sampleUserID);
        restTemplate.put(userUri, sampleUser(roleUri.toString())); // create user linked with role

        Resource<User> user = getUser(userUri);
        assertNotNull(user);

        final URI rolesOfUserUri = new URI(user.getLink("users.User.roles").getHref());
        Resource<List<Resource<Role>>> roles = getRoles(rolesOfUserUri);
        assertNotNull(roles.getContent());
        assertFalse(roles.getContent().isEmpty()); // check if /user/1/roles contains a role
    }

    private String sampleRole() {
        return "{\"roleTitle\":\"admin\"}";
    }

    private final String sampleUserID = "1";

    private String sampleUser(String roleUrl) {
        return "{\"username\":\"god\",\"roles\":[{\"rel\":\"roles\",\"href\":\"" + roleUrl + "\"}]}";
    }

    private Resource<User> getUser(URI uri) {
        return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Resource<User>>() {
        }).getBody();
    }

    private Resource<List<Resource<Role>>> getRoles(URI uri) {
        return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Resource<List<Resource<Role>>>>() {
        }).getBody();
    }
}
