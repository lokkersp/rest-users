package org.noctus;

/**
 * Created by noctuam on 20.09.15.
 */
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

@Configuration
public class WebConfiguration extends RepositoryRestMvcConfiguration{

}
