package org.noctus;

/**
 * Created by noctuam on 20.09.15.
 */
import org.noctus.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path = "users", rel = "users")
public interface IUserRepository extends PagingAndSortingRepository<User, Integer>{

}
