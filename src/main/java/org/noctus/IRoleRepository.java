package org.noctus;

/**
 * Created by noctuam on 20.09.15.
 */
import org.noctus.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path = "roles", rel = "roles")
public interface IRoleRepository extends PagingAndSortingRepository<Role, Long>{
}
