package org.noctus.model;

/**
 * Created by noctuam on 20.09.15.
 */
import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long roleID;
    private String roleTitle;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;


    public List<User> getUserWithRole() {
        return users;
    }

    public void setUserWithRole(List<User> users) {
        this.users = users;
    }


    public Long getRoleID() {
        return roleID;
    }

    public void setRoleID(Long roleID) {
        this.roleID = roleID;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }


}
